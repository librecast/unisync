# unisync (Unicast IoT Updater)

Unicast (TCP/UDP) file syncing test program for comparison with the IPv6
multicast iotupd program during the Librecast Project's Fed4Fire+ experiments to
compare unicast and multicast methods of file syncing.

## Background

Multicast is an under-utilized network technology, often thought of as being
mainly useful for streaming realtime video and audio.  However, multicast has a
much wider range of applications than this, and rather counter-intuitively is
better suited to non-streaming and non-realtime applications such as file
syncing.

Our hypothesis is that the use of multicast in place of unicast can result in
significantly reduced load, bandwidth and infrastructure requirements, reducing
costs, power consumption and carbon savings.

To test this, we ran some simple file syncing experiments comparing unicast (TCP
and UDP) against IPv6 multicast to simulate the deployment of software updates.
We measured the time taken, bandwidth and server load with different payload
sizes and increasing numbers of client nodes and varied scheduling.

The aim was to both demonstrate the advantages of multicast over unicast and to
highlight some of the fundamental differences between the two technologies.

## License

This program is licensed under GPLv2 or (at your option) GPLv3.

## Funding

This project is funded through [NGI Assure](https://nlnet.nl/assure), a fund established by [NLnet](https://nlnet.nl) with financial support from the European Commission's [Next Generation Internet](https://ngi.eu) program. Learn more at the [NLnet project page](https://nlnet.nl/project/LibreCastLiveStudio).

[<img src="https://nlnet.nl/logo/banner.png" alt="NLnet foundation logo" width="20%" />](https://nlnet.nl)
[<img src="https://nlnet.nl/image/logos/NGIAssure_tag.svg" alt="NGI Assure Logo" width="20%" />](https://nlnet.nl/assure)

