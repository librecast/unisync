/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only
 * Copyright (c) 2019-2021 Brett Sheffield <brett@gladserv.com> */

#include <stdint.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <librecast/crypto.h>

#define MAX_CHANNELS 1 /* prime number recommended to ensure full data avail on single channel */
#define LOSS_TOLERANCE 0.05 /* MAX PACKET LOSS % */
#define PKTS_STABILITY 10000    /* number of packets to receive without loss to consider stable */
#define SENDQ_PKTS 100 /* limit number of outbound packets in send queue */

/* IPv6 path discovery isn't much use for multicast and
 * we don't want to receive a bunch of Packet Too Big messages
 * so we'll use a fixed MTU of 1280 - headers + extensions => ~1200 */
#define MTU_FIXED 1194
#define MTU_IPV6 1280

typedef struct iot_file_t {
	off_t	size;			/* Total size, in bytes */
} iot_file_t;

typedef struct iot_frame_t {
	uint16_t	seq;				/* sequence number */
	uint64_t	size;				/* full file size */
	uint64_t	off;				/* offset */
	uint16_t	len;				/* length of this chunk */
	unsigned char	hash[HASHSIZE];			/* SHA3 hash of file */
	char 		data[MTU_FIXED];		/* data */
} __attribute__((__packed__)) iot_frame_t;

typedef struct iot_request_s iot_request_t;
struct iot_request_s {
	uint64_t off;
	uint64_t len;
};
