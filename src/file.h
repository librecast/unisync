/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only
 *
 * this file is part of UNISYNC
 *
 * Copyright (c) 2021 Brett Sheffield <bacs@librecast.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file COPYING in the distribution).
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _FILE_H
#define _FILE_H 1

/* set to 1 to write data to file, 0 to just save it in memory */
#define WRITE_FILE 1

#include <sys/types.h>
#include <sys/stat.h>

int file_map_read(int *fd, struct stat *sb, char **map, char *filename);
#if WRITE_FILE
int file_open_write(int *fd, struct stat *sb, char *filename);
#endif
int file_map_write(int fd, char **map, size_t sz);
void file_unmap(int fd, size_t sz, char *map);

#endif /* _FILE_H */
