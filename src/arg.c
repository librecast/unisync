/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only
 *
 * this file is part of UNISYNC
 *
 * Copyright (c) 2021 Brett Sheffield <bacs@librecast.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file COPYING in the distribution).
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "arg.h"
#include "log.h"
#include "multicast.h"
#include "unicast.h"
#include <string.h>
#include <stdlib.h>

int arg_parse(int *argc, char **argv[])
{
	(void) argc;
	char *mode = (*argv)[ARG_MODE];
	char *proto = (*argv)[ARG_PROTO];
	if (*argc < 5) {
		logmsg(LOG_ERROR, "too few arguments");
		return 0;
	}
	g_file = (*argv)[ARG_FILE];
	g_iface = (*argv)[ARG_IFACE];
	g_src = (*argv)[ARG_SRC];
	if (!strncmp(mode, "server", 1)) {
		g_mode = G_MODE_SERVER;
	}
	else if (!strncmp(mode, "client", 1)) {
		g_mode = G_MODE_CLIENT;
	}
	else {
		logmsg(LOG_ERROR, "unknown mode '%s'", mode);
		return 0;
	}
	if (!strncmp(proto, "multicast", 1)) {
		g_proto = G_PROTO_MULTICAST;
	}
	else if (!strncmp(proto, "udp", 1)) {
		g_proto = G_PROTO_UNICAST_UDP;
	}
	else if (!strncmp(proto, "tcp", 1)) {
		g_proto = G_PROTO_UNICAST_TCP;
	}
	else {
		logmsg(LOG_ERROR, "unknown protocol '%s'", proto);
		return 0;
	}
	if (g_mode == G_MODE_CLIENT) {
		if (g_proto == G_PROTO_MULTICAST) {
			action = &multicast_client;
		}
		else if (g_proto == G_PROTO_UNICAST_TCP) {
			action = &unicast_client_tcp;
		}
		else if (g_proto == G_PROTO_UNICAST_UDP) {
			action = &unicast_client_udp;
		}
	}
	else if (g_mode == G_MODE_SERVER) {
		if (g_proto == G_PROTO_MULTICAST) {
			action = &multicast_server;
		}
		else if (g_proto == G_PROTO_UNICAST_TCP) {
			action = &unicast_server_tcp;
		}
		else if (g_proto == G_PROTO_UNICAST_UDP) {
			action = &unicast_server_udp;
		}
	}
	return 0;
}
