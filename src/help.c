/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only
 *
 * this file is part of UNISYNC
 *
 * Copyright (c) 2021 Brett Sheffield <bacs@librecast.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file COPYING in the distribution).
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "arg.h"
#include "help.h"
#include <libgen.h>
#include <stdio.h>

int help_print(int argc, char *argv[])
{
	(void) argc;
	//printf("usage:\n\t%s client|server multicast|udp file [interface]\n",
	//		basename(argv[ARG_PROGNAME]));
	printf("usage:\n\t%s client|server tcp|udp file srcaddr\n",
			basename(argv[ARG_PROGNAME]));
	return 0;
}
