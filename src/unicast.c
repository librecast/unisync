/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only
 *
 * this file is part of UNISYNC
 *
 * Copyright (c) 2021 Brett Sheffield <bacs@librecast.net>
 * Copyright (c) 2021 Claudio Calvelli <clc@librecast.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file COPYING in the distribution).
 * If not, see <http://www.gnu.org/licenses/>.
 */

#undef USE_SENDFILE

#include "file.h"
#include "globals.h"
#include "iot.h"
#include "log.h"
#include "unicast.h"
#include <assert.h>
#include <arpa/inet.h>
#include <errno.h>
#include <librecast.h>
#include <limits.h>
#include <netdb.h>
#include <pthread.h>
#include <semaphore.h>
#include <signal.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#ifdef USE_SENDFILE
#include <sys/sendfile.h>
#endif
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <sys/param.h>
#include <sys/un.h>
#ifdef USE_LWMON
#include <lwmon/packet.h>
#endif

#ifndef USE_SENDFILE
#define SEND_BLOCK 4096
#endif

#define LWMON_PATH "/tmp/lwmon.socket"

static volatile sig_atomic_t running = 1;
static volatile sig_atomic_t do_client_request;
static volatile uint64_t client_offset = 0;
static sem_t sem;
static unsigned char hash[HASHSIZE];
static unsigned char * volatile bitmap = NULL;
static size_t maplen;
static volatile sig_atomic_t have_bitmap;

static void handle_sigint(int signo)
{
	(void) signo;
	running = 0;
}

static void *get_in_addr(struct sockaddr *sa)
{
	return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

/* return number of bits set in bitmap (Hamming Weight) */
static unsigned int hamm(unsigned char *map, size_t len)
{
	unsigned int c = 0;
	while (len--) for (char v = map[len]; v; c++) v &= v - 1;
	return c;
}

/* log an event with lwmon if possible */
#ifdef USE_LWMON
static void lwmon_log(const char * who, const char * what)
{
	static int lwmon_fd = -1;
	lwmon_packet_t P;
	if (lwmon_fd == -1) {
		struct sockaddr_un ls = {
			.sun_family = AF_UNIX,
			.sun_path   = LWMON_PATH,
		};
		int fd = socket(AF_UNIX, SOCK_DGRAM, 0);
		if (fd == -1) return;
		if (connect(fd, (struct sockaddr *)&ls, sizeof(ls)) == -1) {
			close(fd);
			return;
		}
		lwmon_fd = fd;
	}
	lwmon_initpacket(&P);
	lwmon_add_log(&P, who, what);
	lwmon_add_command(&P, lwmon_command_measure);
	lwmon_closepacket(&P);
	lwmon_sendpacket(lwmon_fd, &P);
}
#endif

int unicast_client_tcp(int argc, char *argv[])
{
	(void) argc; (void) argv;
#if WRITE_FILE
	int fd;
	struct stat sb;
#else
	const int fd = -1;
#endif
	char *map = NULL;
	int sock = -1;
	struct addrinfo hints = {0};
	struct addrinfo *a = NULL;
	struct addrinfo *ai = NULL;
	struct addrinfo *p;
	char *ptr;
	char hex[HEXLEN];
	unsigned char srchash[HASHSIZE];
	unsigned char dsthash[HASHSIZE];
	uint64_t sz;
	ssize_t byt;
	ssize_t tot = 0;


#if WRITE_FILE
	if (file_open_write(&fd, &sb, g_file) == -1) {
		return -1;
	}
#endif
	logmsg(LOG_DEBUG, "client connecting to [%s]:%s", g_src, g_port);
#ifdef USE_LWMON
	lwmon_log("client_tcp", "start");
#endif
	do {

		hints.ai_family = AF_INET6;
		hints.ai_flags = AI_PASSIVE;
		hints.ai_socktype = SOCK_STREAM;
		for (int e = getaddrinfo(g_src, g_port, &hints, &a); a; a = a->ai_next) {
			if (e) {
				logmsg(LOG_ERROR, "getaddrinfo: %s", strerror(e));
				goto exit_err_0;
			}
			if (!ai) ai = a;
			if ((sock = socket(a->ai_family, a->ai_socktype, a->ai_protocol)) == -1)
				continue;
			for (p = a; p != NULL; p = p->ai_next) {
				if (connect(sock, p->ai_addr, p->ai_addrlen) == -1) {
					logmsg(LOG_ERROR, "connect: %s", strerror(errno));
					close(sock);
					goto exit_err_0;
				}
			}
			break;
		}
		freeaddrinfo(ai);
		logmsg(LOG_DEBUG, "client connected");

		/* read size */
		recv(sock, &sz, sizeof(uint64_t), 0);
		sz = be64toh(sz);

		/* read checksum */
		recv(sock, srchash, HASHSIZE, 0);
		sodium_bin2hex(hex, HEXLEN, srchash, HASHSIZE);
		logmsg(LOG_DEBUG, "src: %s (%li bytes)", hex, (long int)sz);

		if (!map && file_map_write(fd, &map, (size_t)sz) == -1) {
			close(sock);
			goto exit_err_0;
		}

		ptr = map;
		while ((byt = recv(sock, ptr, sz-(size_t)tot, 0)) != -1 && byt) {
			logmsg(LOG_DEBUG, "%lu bytes read", byt);
			tot += byt;
			ptr += byt;
		}
		if (byt == -1) perror("recv");
		close(sock);

		/* verify file hash */
		hash_generic(dsthash, HASHSIZE, (unsigned char *)map, sz);
		sodium_bin2hex(hex, HEXLEN, dsthash, HASHSIZE);
		logmsg(LOG_DEBUG, "dst: %s (%lu bytes)", hex, tot);
	}
	while (memcmp(dsthash, srchash, HASHSIZE));

exit_err_0:
#ifdef USE_LWMON
	lwmon_log("client_tcp", "end");
#endif
	file_unmap(fd, sz, map);
	return 0;
}

int unicast_server_tcp(int argc, char *argv[])
{
	(void) argc; (void) argv;
	int fd;
	struct stat sb;
	char *map = NULL;
	struct sigaction sa_int = { .sa_handler = handle_sigint };
	struct sigaction sa_chld = { .sa_handler = SIG_IGN };
	struct addrinfo hints = {0};
	struct addrinfo *a = NULL;
	struct addrinfo *ai = NULL;
	struct sockaddr_storage addr;
	char straddr[INET6_ADDRSTRLEN];
	unsigned char hash[HASHSIZE];
	socklen_t addrlen = sizeof (struct sockaddr_storage);
	int sock = -1;
	int yes = 1;
	int fdclient;
#ifdef USE_SENDFILE
	off_t off = 0;
#endif
	uint64_t sz;
	ssize_t byt = 0;
	ssize_t tot = 0;

	if (file_map_read(&fd, &sb, &map, g_file) == -1) {
		return -1;
	}

	hints.ai_family = AF_INET6;
	hints.ai_flags = AI_PASSIVE;
	hints.ai_socktype = SOCK_STREAM;
	for (int e = getaddrinfo(g_src, g_port, &hints, &a); a; a = a->ai_next) {
		if (e) {
			logmsg(LOG_ERROR, "getaddrinfo: %s", strerror(e));
			goto exit_err_0;
		}
		if (!ai) ai = a;
		if ((sock = socket(a->ai_family, a->ai_socktype, a->ai_protocol)) == -1)
			continue;
		if ((setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int))) == -1)
			continue;
		if ((bind(sock, a->ai_addr, a->ai_addrlen)) == -1)
			continue;
		break;
	}
	freeaddrinfo(ai);
	if (sock != -1) {
		logmsg(LOG_DEBUG, "Listening on [%s]:%s", g_src, g_port);
		if ((listen((sock), BACKLOG)) == -1) {
			logmsg(LOG_ERROR, "listen() error: %s", strerror(errno));
			goto exit_err_0;
		}
	}

	sigaction(SIGCHLD, &sa_chld, NULL);
	sigaction(SIGINT, &sa_int, NULL);
	sigaction(SIGTERM, &sa_int, NULL);

	/* calculate file hash */
	hash_generic(hash, HASHSIZE, (unsigned char *)map, sb.st_size);

#ifdef USE_LWMON
	lwmon_log("server_tcp", "start");
#endif
	while (running) {
		fdclient = accept(sock, (struct sockaddr *)&addr, &addrlen);
		if (fdclient != -1) {
			inet_ntop(AF_INET6, get_in_addr((struct sockaddr *)&addr), straddr, INET6_ADDRSTRLEN);
			logmsg(LOG_DEBUG, "connection received from %s", straddr);
#ifdef USE_LWMON
			lwmon_log("server_tcp", "request");
#endif
			if (!fork()) {
				sz = htobe64((uint64_t)sb.st_size);
				send(fdclient, &sz, sizeof(uint64_t), 0);
				send(fdclient, hash, HASHSIZE, 0);
#ifdef USE_SENDFILE
				off = 0;
#endif
				while (tot < (ssize_t)sb.st_size) {
#ifdef USE_SENDFILE
					byt = sendfile(fdclient, fd, &off, sb.st_size);
#else
					size_t len = sb.st_size - tot;
					if (len > SEND_BLOCK) len = SEND_BLOCK;
					byt = send(fdclient, &map[tot], SEND_BLOCK, 0);
#endif
					if (byt == -1) {
#ifdef USE_SENDFILE
						perror("sendfile");
#else
						perror("send");
#endif
						break;
					}
					tot += byt;
				}
#ifdef USE_LWMON
				lwmon_log("server_tcp", "done");
#endif
				exit(0);
			}
		}
		close(fdclient);
	}

exit_err_0:
#ifdef USE_LWMON
	lwmon_log("server_tcp", "end");
#endif
	file_unmap(fd, sz, map);
	return 0;
}

static int unicast_socket_udp(const char *node, const char *service,
		const struct addrinfo *hints,
		struct addrinfo **a,
		struct addrinfo **ai)
{
	int sock = -1;
	for (int e = getaddrinfo(node, service, hints, a); *a; *a = (*a)->ai_next) {
		if (e) {
			logmsg(LOG_ERROR, "getaddrinfo: %s", strerror(e));
			return -1;
		}
		if (!(*ai)) *ai = *a;
		if ((sock = socket((*a)->ai_family, (*a)->ai_socktype, (*a)->ai_protocol)) == -1)
			continue;
		break;
	}
	return sock;
}

static void *unicast_client_udp_listen(void *arg)
{
	int sock = *(int *)arg;
	struct sockaddr_storage addr;
	socklen_t addrlen = sizeof addr;
	char straddr[INET6_ADDRSTRLEN] = "";
	char buf[BUFSIZ];
	size_t byt = 0;
	size_t len;
	size_t pkts, idx;
	size_t pktsin = 0;
	ssize_t rc;
	iot_frame_t * res;
	char *map = NULL;
	char hex[HEXLEN];
	uint64_t sz = 0;
	uint64_t bwrit = 0;
	uint64_t off;
	uint8_t mod;
#if WRITE_FILE
	struct stat sb;
	int fd;
#else
	const int fd = -1;
#endif

#if WRITE_FILE
	if (file_open_write(&fd, &sb, g_file) == -1) {
		_exit(1);
	}
#endif
	while (running) {
		rc = recvfrom(sock, buf, BUFSIZ, 0, (struct sockaddr *)&addr, &addrlen);
		if (rc == -1) {
			perror("recvfrom");
			break;
		}
		inet_ntop(AF_INET6, get_in_addr((struct sockaddr *)&addr), straddr, INET6_ADDRSTRLEN);
		res = (iot_frame_t *)buf;
		sz = be64toh(res->size);
		if (!map) {
			if (file_map_write(fd, &map, (size_t)sz) == -1) {
				close(sock);
				_exit(1);
			}
			/* allocate bitmap */
			pkts = howmany(sz, MTU_FIXED);
			logmsg(LOG_DEBUG, "allocating bitmap - pkts expected: %zu", pkts);
			maplen = howmany(pkts, CHAR_BIT);
			bitmap = malloc(maplen); assert(bitmap);
			memset(bitmap, ~0, maplen);
			mod = pkts % CHAR_BIT;
			if (mod) bitmap[maplen - 1] = (1U << (mod)) - 1;
			have_bitmap = 1;
		}
		len = (size_t)be16toh(res->len);
		//DEBUG("%zi bytes (%zu byte payload) received from %s", rc, len, straddr);
		byt += len;
		off = be64toh(res->off);
		idx = off / MTU_FIXED;
		if (isset(bitmap, idx)) {
			memcpy(map + off, res->data, len);
			clrbit(bitmap, idx);
			bwrit += len;
			msync(map, (size_t)sz, MS_ASYNC);
			if (sz <= bwrit) {
				/* calculate file hash */
				hash_generic(hash, HASHSIZE, (unsigned char *)map, sz);
				if (!memcmp(hash, res->hash, HASHSIZE)) {
					sodium_bin2hex(hex, HEXLEN, res->hash, HASHSIZE);
					logmsg(LOG_DEBUG, "src: %s (%lu bytes)", hex, sz);
					sodium_bin2hex(hex, HEXLEN, hash, HASHSIZE);
					logmsg(LOG_DEBUG, "dst: %s (%lu bytes)", hex, bwrit);
					running = 0;
				}
				else {
					do_client_request++;
				}
			}
		}
		else {
			DEBUG("dropping data we already have with idx %zu", idx);
		}
		pktsin++;
		sem_post(&sem);
	}
	logmsg(LOG_DEBUG, "%zu pkts received", pktsin);
	logmsg(LOG_DEBUG, "%zu bytes received", byt);
	logmsg(LOG_DEBUG, "%lu bytes written", bwrit);
	file_unmap(fd, sz, map);
	free(bitmap);
	return arg;
}

static ssize_t send_udp_request(int sock, struct addrinfo *a, uint16_t reqc, iot_request_t *req[])
{
	struct iovec iov[2] = {0};
	struct msghdr msg = {
		.msg_name = a->ai_addr,
		.msg_namelen = a->ai_addrlen,
		.msg_iov = iov,
		.msg_iovlen = 2
	};
	iov[1].iov_base = req;
	iov[1].iov_len = sizeof(iot_request_t) * reqc;
	reqc = htons(reqc);
	iov[0].iov_base = &reqc;
	iov[0].iov_len = sizeof reqc;
	ssize_t rc;
	rc = sendmsg(sock, &msg, 0);
	if (rc == -1) perror("sendmsg");
	//else { DEBUG("request of size %zi sent", rc); }
	return rc;
}

static int request_missing(int sock, struct addrinfo *a, unsigned int missing, int holes)
{
	/* maximum number of requests per packet */
	const int maxreq = (MTU_IPV6 - sizeof(uint16_t)) / sizeof(iot_request_t);
	iot_request_t req[maxreq];
	ssize_t start = -1;
	ssize_t rc = 0;
	int c = 0;
	int reqc = 0;
	logmsg(LOG_DEBUG, "requesting missing %u packets (%i holes)", missing, holes);
	for (size_t z = 0; z < maplen; z++) {
		if (bitmap[z] || start >= 0) {
			for (size_t y = 0; y < CHAR_BIT; y++) {
				size_t bit = z * CHAR_BIT + y;
				if (start >= 0 && !isset(bitmap, bit)) {
					size_t len = (bit - start) * MTU_FIXED;
					req[c].off = (uint64_t)htobe64(start * MTU_FIXED);
					req[c].len = (uint64_t)htobe64(len);
					c++;
					if (c == maxreq) {
						rc = send_udp_request(sock, a, c, (iot_request_t **)&req);
						c = 0;
						if (reqc++ > REQUEST_MAX) return rc;
					}
					start = -1;
				}
				else if (start < 0 && isset(bitmap, bit)) {
					start = bit;
				}
			}
		}
	}
	if (start >= 0) {
		/* hole extends to end of file */
		req[c].off = (uint64_t)start;
		req[c].len = 0;
		c++;
	}
	rc = send_udp_request(sock, a, c, (iot_request_t **)&req);
	return rc;
}

static int count_holes(void)
{
	ssize_t start = -1;
	int holes = 0;
	for (size_t z = 0; z < maplen; z++) {
		if (bitmap[z] || start >= 0) {
			for (size_t y = 0; y < CHAR_BIT; y++) {
				size_t bit = z * CHAR_BIT + y;
				if (start >= 0 && !isset(bitmap, bit)) {
					start = -1;
					holes++;
				}
				else if (start < 0 && isset(bitmap, bit)) {
					start = bit;
				}
			}
		}
	}
	if (start >= 0) {
		/* hole extends to end of file */
		holes++;
	}
	return holes;
}

int unicast_client_udp(int argc, char *argv[])
{
	(void) argc; (void) argv;
	enum {
		SOCK_IN,
		SOCK_OUT,
		SOCKS
	};
	struct addrinfo hints = {0};
	struct addrinfo *a[2] = {0};
	struct addrinfo *ai[2] = {0};
	struct sigaction sa_int = { .sa_handler = handle_sigint };
	struct sigaction sa_chld = { .sa_handler = SIG_IGN };
	struct timespec ts;
	ssize_t rc = 0;
	int sz = 0;
	socklen_t optlen = sizeof sz;
	int sock[2] = {0};
	int yes = 1;

	hints.ai_family = AF_INET6;
	hints.ai_socktype = SOCK_DGRAM;

	sock[SOCK_OUT] = unicast_socket_udp(g_src, g_port, &hints, &a[SOCK_OUT], &ai[SOCK_OUT]);
	if (sock[SOCK_OUT] == -1) {
		logmsg(LOG_ERROR, "unable to create outbound socket");
		goto exit_err_0;
	}

	hints.ai_flags = AI_PASSIVE;
	sock[SOCK_IN] = unicast_socket_udp(NULL, "4243", &hints, &a[SOCK_IN], &ai[SOCK_IN]);
	if (sock[SOCK_IN] == -1) {
		logmsg(LOG_ERROR, "unable to create inbound socket");
		goto exit_err_0;
	}
	if (getsockopt(sock[SOCK_IN], SOL_SOCKET, SO_RCVBUF, &sz, &optlen) == -1) {
		perror("getsockopt(SO_RCVBUF)");
	}
	DEBUG("socket buffer was %i bytes", sz);
	sz = 2147483646;
	if (setsockopt(sock[SOCK_IN], SOL_SOCKET, SO_RCVBUFFORCE, &sz, sizeof sz) == -1) {
		perror("setsockopt(SO_RCVBUF)");
	}
	sz = 0;
	if (getsockopt(sock[SOCK_IN], SOL_SOCKET, SO_RCVBUF, &sz, &optlen) == -1) {
		perror("getsockopt(SO_RCVBUF)");
	}
	DEBUG("socket buffer now %i bytes", sz);
	if ((setsockopt(sock[SOCK_IN], SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int))) == -1) {
		perror("setsockopt");
		goto exit_err_0;
	}
	if ((bind(sock[SOCK_IN], a[SOCK_IN]->ai_addr, a[SOCK_IN]->ai_addrlen)) == -1) {
		perror("bind");
		goto exit_err_0;
	}

	sigaction(SIGCHLD, &sa_chld, NULL);
	sigaction(SIGINT, &sa_int, NULL);
	sigaction(SIGTERM, &sa_int, NULL);

#ifdef USE_LWMON
	lwmon_log("client_udp", "start");
#endif

	pthread_attr_t attr;
	pthread_t tlisten;
	sem_init(&sem, 0, 0);
	pthread_attr_init(&attr);
	pthread_create(&tlisten, &attr, &unicast_client_udp_listen, &sock[SOCK_IN]);
	pthread_attr_destroy(&attr);
	while (running) {
		if (have_bitmap) {
			unsigned int missing = hamm(bitmap, maplen);
			do_client_request = 0;
			if (missing) {
				rc = request_missing(sock[SOCK_OUT], a[SOCK_OUT], missing, count_holes());
			}
		}
		else {
			iot_request_t req[1] = {{0}};
			rc = send_udp_request(sock[SOCK_OUT], a[SOCK_OUT], 1, (iot_request_t **)&req);
			DEBUG("request of size %zi sent", rc);
			logmsg(LOG_DEBUG, "sending request to [%s]:%s", g_src, g_port);
		}

		do {
			clock_gettime(CLOCK_REALTIME, &ts);
			ts.tv_sec += REQUEST_TIMEOUT;
			ts.tv_nsec += REQUEST_TIMEOUT_NS;
			while (ts.tv_nsec > 999999999) {
				ts.tv_nsec -= 999999999;
				ts.tv_sec++;
			}
		} while ((rc = sem_timedwait(&sem, &ts)) == 0 && !do_client_request);
		if (rc && errno != ETIMEDOUT && errno != EINTR) {
			perror("sem_timedwait");
			running = 0;
		}
	}
	sem_destroy(&sem);

exit_err_0:
#ifdef USE_LWMON
	lwmon_log("client_udp", "end");
#endif
	freeaddrinfo(ai[SOCK_OUT]);
	freeaddrinfo(ai[SOCK_IN]);
	close(sock[SOCK_OUT]);
	close(sock[SOCK_IN]);
	return 0;
}

static void unicast_handle_udp_request(int sock, char *buf, struct sockaddr *addr, socklen_t addrlen, char *map, size_t mlen)
{
	iot_request_t *req = (iot_request_t *)buf;
	iot_frame_t res = {0};
	size_t plen;
	uint64_t reqoff = be64toh(req->off);
	uint64_t reqlen = be64toh(req->len);
	size_t bytout = 0;
	int rc, pkts = 0;
	//char straddr[INET6_ADDRSTRLEN] = "";

	//inet_ntop(AF_INET6, get_in_addr((struct sockaddr *)addr), straddr, INET6_ADDRSTRLEN);

	if (!reqlen) reqlen = mlen - reqoff;

	//DEBUG("processing record request for %zu bytes", reqlen);

	((struct sockaddr_in6 *)addr)->sin6_port = htons(4243);

	//DEBUG("off = %lu, mlen = %lu", reqoff, mlen);
	for (size_t off = reqoff; running && off < mlen; off += MTU_FIXED) {
		plen = ((off + MTU_FIXED) > mlen) ? mlen - off : MTU_FIXED;
		res.size = htobe64(mlen);
		res.off = htobe64((uint64_t)off);
		res.len = htobe16((uint64_t)plen);
		memcpy(res.hash, hash, HASHSIZE);
		memcpy(res.data, map + off, plen);
		//DEBUG("sending data off=%zu", off);
		rc = sendto(sock, &res, sizeof res, 0, addr, addrlen);
		if (rc == -1) {
			perror("sendto");
			break;
		}
		pkts++;
		bytout += plen;
		if (bytout >= reqlen) break;
	}
	//if (bytout) logmsg(LOG_DEBUG, "%zu bytes sent in %i packets to %s", bytout, pkts, straddr);
}

static void unicast_handle_udp_requests(int sock, uint16_t reqc, char *buf, struct sockaddr *addr, socklen_t addrlen, char *map, size_t mlen)
{
	char *ptr = buf;
	char straddr[INET6_ADDRSTRLEN] = "";
	inet_ntop(AF_INET6, get_in_addr((struct sockaddr *)addr), straddr, INET6_ADDRSTRLEN);
	DEBUG("request received from %s with %u records", straddr, reqc);

	for (uint16_t u = 0; u < reqc; u++) {
		unicast_handle_udp_request(sock, ptr, addr, addrlen, map, mlen);
		ptr += sizeof(iot_request_t);
	}
#ifdef USE_LWMON
	lwmon_log("server_udp", "done");
#endif
	close(sock);
	_exit(0);
}

int unicast_server_udp(int argc, char *argv[])
{
	(void) argc; (void) argv;
	enum {
		SOCK_IN,
		SOCK_OUT,
		SOCKS
	};
	uint64_t sz = 0;
	struct stat sb;
	struct addrinfo hints = {0};
	struct addrinfo *a[2] = {0};
	struct addrinfo *ai[2] = {0};
	struct sockaddr_storage addr = {0};
	socklen_t addrlen = sizeof addr;
	struct sigaction sa_int = { .sa_handler = handle_sigint };
	struct sigaction sa_chld = { .sa_handler = SIG_IGN };
	char *map = NULL;
	char buf[BUFSIZ] = {0};
	int fd;
	ssize_t rc, expected;
	int sock[SOCKS] = {0};
	int yes = 1;
	ssize_t byt = 0;

	if (file_map_read(&fd, &sb, &map, g_file) == -1) {
		return -1;
	}
	hints.ai_family = AF_INET6;
	hints.ai_socktype = SOCK_DGRAM;

	sock[SOCK_OUT] = unicast_socket_udp(NULL, "4243", &hints, &a[SOCK_OUT], &ai[SOCK_OUT]);
	if (sock[SOCK_OUT] == -1) {
		logmsg(LOG_ERROR, "unable to create outbound socket");
		goto exit_err_0;
	}
	logmsg(LOG_DEBUG, "Listening on [%s]:%s", g_src, g_port);

	hints.ai_flags = AI_PASSIVE;
	sock[SOCK_IN] = unicast_socket_udp(g_src, g_port, &hints, &a[SOCK_IN], &ai[SOCK_IN]);
	if (sock[SOCK_IN] == -1) {
		logmsg(LOG_ERROR, "unable to create inbound socket");
		goto exit_err_0;
	}
	if ((setsockopt(sock[SOCK_IN], SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int))) == -1) {
		perror("setsockopt");
		goto exit_err_0;
	}
	if ((bind(sock[SOCK_IN], a[SOCK_IN]->ai_addr, a[SOCK_IN]->ai_addrlen)) == -1) {
		perror("bind");
		goto exit_err_0;
	}

	sigaction(SIGCHLD, &sa_chld, NULL);
	sigaction(SIGINT, &sa_int, NULL);
	sigaction(SIGTERM, &sa_int, NULL);

	/* calculate file hash */
	hash_generic(hash, HASHSIZE, (unsigned char *)map, sb.st_size);
#ifdef USE_LWMON
	lwmon_log("server_udp", "start");
#endif

	struct iovec iov[2];
	struct msghdr msg = {
		.msg_name = &addr,
		.msg_namelen = addrlen,
		.msg_iov = iov,
		.msg_iovlen = 2
	};
	uint16_t reqc;
	iov[0].iov_base = &reqc;
	iov[0].iov_len = sizeof reqc;
	iov[1].iov_base = buf;
	iov[1].iov_len = BUFSIZ;
	while (running) {
		rc = recvmsg(sock[SOCK_IN], &msg, 0);
		reqc = ntohs(reqc);
#ifdef USE_LWMON
		lwmon_log("server_udp", "request");
#endif
		if (rc == -1) {
			if (errno != EINTR) {
				perror("recvfrom");
				_exit(1);
			}
			break;
		}
		if (rc > 0) byt += rc;
		expected = reqc * sizeof(iot_request_t) + sizeof reqc;
		if (rc != expected) {
			ERROR("Request is wrong length. Expected %zi, got %zi", expected, rc);
			continue;
		}
		if (!fork()) {
			unicast_handle_udp_requests(sock[SOCK_OUT], reqc, buf, (struct sockaddr *)&addr, addrlen, map, sb.st_size);
		}
	}
	logmsg(LOG_DEBUG, "%zi bytes received", byt);

exit_err_0:
#ifdef USE_LWMON
	lwmon_log("server_udp", "end");
#endif
	file_unmap(fd, sz, map);
	freeaddrinfo(ai[SOCK_OUT]);
	freeaddrinfo(ai[SOCK_IN]);
	close(sock[SOCK_OUT]);
	close(sock[SOCK_IN]);
	return 0;
}
