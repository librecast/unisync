/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only
 *
 * this file is part of UNISYNC
 *
 * Copyright (c) 2021 Brett Sheffield <bacs@librecast.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file COPYING in the distribution).
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "file.h"
#include "log.h"

#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <unistd.h>

int file_map_read(int *fd, struct stat *sb, char **map, char *filename)
{
	if ((*fd = open(filename, O_RDONLY)) == -1) {
		logmsg(LOG_ERROR, "Unable to open file '%s'", filename);
		return -1;
	}
	if (fstat(*fd, sb) == -1) {
		logmsg(LOG_ERROR, "fstat() failed");
		return -1;
	}

	*map = mmap(NULL, sb->st_size, PROT_READ, MAP_SHARED, *fd, 0);
	if (*map == MAP_FAILED) {
		logmsg(LOG_ERROR, "mmap() failed: %s", strerror(errno));
		return -1;
	}

	return 0;
}

#if WRITE_FILE
int file_open_write(int *fd, struct stat *sb, char *filename)
{
	if ((*fd = open(filename, O_CREAT|O_RDWR, S_IRUSR|S_IWUSR)) == -1) {
		logmsg(LOG_ERROR, "Unable to open file '%s'", filename);
		return -1;
	}
	if (fstat(*fd, sb) == -1) {
		logmsg(LOG_ERROR, "fstat() failed");
		return -1;
	}
	return 0;
}
#endif

int file_map_write(int fd, char **map, size_t sz)
{
	if (fd != -1 && ftruncate(fd, sz) == -1) {
		logmsg(LOG_ERROR, "ftruncate() failed: %s", strerror(errno));
		return -1;
	}
	logmsg(LOG_DEBUG, "ftruncate()ed file to size %zu", sz);
	if (fd == -1)
		*map = mmap(NULL, sz, PROT_READ|PROT_WRITE, MAP_ANONYMOUS, fd, 0);
	else
		*map = mmap(NULL, sz, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
	if (*map == MAP_FAILED) {
		logmsg(LOG_ERROR, "mmap() failed: %s", strerror(errno));
		return -1;
	}
	return 0;
}

void file_unmap(int fd, size_t sz, char *map)
{
	if (map) munmap(map, sz);
	if (fd != -1) close(fd);
}
