/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only
 *
 * this file is part of UNISYNC
 *
 * Copyright (c) 2021 Brett Sheffield <bacs@librecast.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file COPYING in the distribution).
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _GLOBALS_H
#define _GLOBALS_H 1

#define BACKLOG 100
#define MTU_FIXED 1194
#define REQUEST_TIMEOUT 1 /* seconds to wait for response to request */
#define REQUEST_TIMEOUT_NS 0 /* nanoseconds to wait for response to request */
#define REQUEST_MAX 10 /* maximum number of re-request packets to send at once */

enum {
	G_MODE_SERVER,
	G_MODE_CLIENT
};
enum {
	G_PROTO_MULTICAST,
	G_PROTO_UNICAST_UDP,
	G_PROTO_UNICAST_TCP
};

extern char * g_src;
extern char * g_port;
extern int g_mode;
extern int g_proto;
extern char * g_file;
extern char * g_iface;

extern int (*action)(int argc, char *argv[]);

#endif /* _GLOBALS_h */
